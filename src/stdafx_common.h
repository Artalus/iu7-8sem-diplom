#pragma once

#include <glm/glm.hpp>

extern float half_pi;
extern float pi;
extern float two_pi;

bool FloatsEqual(float a, float b, float eps = 1e-6f);
	