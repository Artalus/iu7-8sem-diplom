#pragma once

#include <array>

#include "IAngleSolver.h"

namespace Arm
{
	// работает со строго двухзвенным манипулятором, в лоб решая кинематическое уравнение
	class AnalyticalSolver : public IAngleSolver
	{
	public:

		//расчёт углов для случая, когда манипулятор работает в плоскости XY и растёт из (0;0)
		static std::array<float, 2> solve2d(const glm::vec2 &target, float l1, float l2);

		AnglesTuple SolveAngles(const Element &chainStart, const glm::vec3 &target) const override;
	};
}
