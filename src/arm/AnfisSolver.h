#pragma once

#include "IAngleSolver.h"
#include <glm/vec3.hpp>

#include <anfisLib.h>

namespace Arm
{
	class AnfisSolver : public IAngleSolver
	{
		mwArray fisYaw{ 1,1,mxSTRUCT_CLASS },
			fisTh1{ 1,1,mxSTRUCT_CLASS },
			fisTh2{ 1,1,mxSTRUCT_CLASS };
		bool ready = false;
	public:
		bool IsReady() { return ready; }

		//загружает структуру FIS из <filename>.fis
		void LoadFis(std::string filenameYaw, std::string filenameTh1, std::string filenameTh2);
		AnglesTuple SolveAngles(const Element &chainStart, const glm::vec3 &target) const override;
	};
}
