#pragma once

#include <windows.h>
#include <gl/gl.h>
#include <glm/glm.hpp>
#include <memory>

namespace Arm
{
	#define SIGN(x) ( (x)>0 ? 1 : ((x)<0 ? -1 : 0) )

	union color
	{
		uint32_t u32;
		GLubyte bytes[4];
	};

	// абстрактный элемент манипулятора
	// самостоятельно следит за растущими из него элементами
	class Element
	{
	protected:
		enum class State { Idle, Rotating } state = State::Idle;
		static const float rotationPrecision;

		Element * const parent;
		std::unique_ptr<Element> child{nullptr};

	public:

		// TODO: проставлять parent-child зависимости через AddChild<Type>(Args&&...)
		Element(Element *parent)
			: parent(parent)
		{
			if ( parent)
				parent->child.reset(this);
		}
		virtual ~Element()
		{
		}

		Element(const Element&) = default;
		Element(Element&&) = default;
		Element& operator=(const Element&) = default;
		Element& operator=(Element&&) = default;

		Element* Parent() const { return parent; }
		Element* Child() const { return child.get(); }

		virtual glm::vec3 GetPos() const = 0;
		virtual float GetYaw() const = 0;
		virtual float GetPitch() const = 0;
		virtual float GetRoll() const = 0;
		virtual glm::vec3 GetAttachmentPoint() const = 0;
		virtual glm::vec3 GetAttachmentAxis() const = 0;

		virtual void Update(const float timeDelta) = 0;
		virtual void Render() const = 0;

		virtual void RotateAt(const float targetAngle, const float speed) = 0;
	};
}