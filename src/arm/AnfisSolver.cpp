#include <stdafx.h>

#include "AnfisSolver.h"

#include "basement.h"
#include "link.h"


using namespace glm;
using namespace std;
using namespace Arm;


void AnfisSolver::LoadFis(string filenameYaw, string filenameTh1, string filenameTh2)
{
	ready = false;

	loadFis(1, fisYaw, filenameYaw.c_str());
	loadFis(1, fisTh1, filenameTh1.c_str());
	loadFis(1, fisTh2, filenameTh2.c_str());
	
	ready = true;
}

AnglesTuple AnfisSolver::SolveAngles(const Element &chainStart, const vec3 &target) const
{
	if ( !ready )
		throw std::exception("Trying to solve angles with no FIS loaded");

	mwArray xyz(3, 1, mxDOUBLE_CLASS);
	xyz(1) = target.x;
	xyz(2) = target.y;
	xyz(3) = target.z;

	mwArray yaw, th1, th2;// (1, 1, mxDOUBLE_CLASS);
	predictAngle(1, yaw, xyz, fisYaw);
	predictAngle(1, th1, xyz, fisTh1);
	predictAngle(1, th2, xyz, fisTh2);

	float y = yaw(1, 1);
	float t1 = th1(1, 1);
	float t2 = th2(1, 1);

	return {y, t1, t2};
}
