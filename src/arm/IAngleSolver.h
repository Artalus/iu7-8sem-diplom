#pragma once

#include <glm/fwd.hpp>
#include "utils/AnglesTuple.h"

namespace Arm
{
	class Element;

	// изучает положение манипулятора, целевую точку, и решает, какие углы должны принять
	// звенья, чтобы совместить рабочую точку и цель
	class IAngleSolver
	{
	public:
		virtual ~IAngleSolver() {}
		virtual AnglesTuple SolveAngles(const Element &chainStart, const glm::vec3 &target) const = 0;
	};

}
