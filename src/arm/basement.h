#pragma once

#include "element.h"

namespace Arm
{
	class Basement : public Element
	{
		glm::vec3 pos;
		const glm::vec3 attachment;
		const glm::vec3 dimensions{ 2.0f, 1.0f, 1.0f };
		float yaw = 0;
		color clr;

	public:
		float targetYaw = 0;
		float yawSpeed = 0;

		Basement(const glm::vec3 &position, const glm::vec3 &attachmentPoint);

		glm::vec3 GetPos() const override;
		float GetYaw() const override;
		float GetPitch() const override;
		float GetRoll() const override;
		glm::vec3 GetAttachmentPoint() const override;
		glm::vec3 GetAttachmentAxis() const override;

		void Update(const float timeDelta) override;

		void Render() const override;

		void RotateAt(const float targetAngle, const float speed) override;
	};
}