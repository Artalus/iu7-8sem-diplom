#pragma once

#include "element.h"

namespace Arm
{
	class AllowedAngle
	{
	public:
		const float minRad, maxRad;
		AllowedAngle(const float minDegrees, const float maxDegrees) :
			minRad( glm::radians(minDegrees))
			, maxRad( glm::radians(maxDegrees))
		{
			assert(minDegrees < maxDegrees);
		}
		bool Check(const float radians) const
		{
			return (minRad <= radians) && (radians <= maxRad);
		}
		float Correct(const float radians) const
		{
			if (radians < minRad)
				return minRad;
			if (radians > maxRad)
				return maxRad;
			return radians;
		}
	};

	class Link : public Element
	{
		const float length;
		float pitch = 0;
		color clr;

	public:
		const AllowedAngle allowedAngle;
		float targetPitch = 0;
		float pitchSpeed = 0;

		Link(Element *parent, const float length, const AllowedAngle &allowedAngle = {-80, 80});
		virtual ~Link();
		glm::vec3 GetPos() const override;
		float GetYaw() const override;
		float GetPitch() const override;
		float GetRoll() const override;
		float GetLength() const;
		glm::vec3 GetAttachmentPoint() const override;
		glm::vec3 GetAttachmentAxis() const override;
		void Update(const float timeDelta) override;
		void Render() const override;

		void RotateAt(const float targetAngle, const float speed) override;

		void SetPitch(const float p);
	};
}