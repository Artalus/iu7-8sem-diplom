#include <stdafx.h>

#include "AnalyticalSolver.h"

#include "element.h"
#include <cassert>
#include "link.h"
#include "basement.h"

#include <cmath>
#include <glm/gtx/vector_angle.hpp>

namespace Arm
{
	using namespace glm;

	// TODO: в класс углов
	float normalizeAngle(float radians)
	{
		while ( radians > ::two_pi )
			radians -= ::two_pi;
		while ( radians < -::two_pi )
			radians += ::two_pi;

		if ( radians > ::pi )
			radians = -::two_pi + radians;
		else if ( radians < -::pi )
			radians = ::two_pi + radians;

		return radians;
	}

	std::array<float, 2> AnalyticalSolver::solve2d(const glm::vec2 &target, float l1, float l2)
{
		/* TODO: пока работает только для треугольников вида
		. *C
		.  /
		A*---*B
		где A основание манипулятора и C обязательно выше
		*/
		// TODO: обработка случаев, когда точка за пределами дальности
		// TODO: обработка случаев, когда результирующие углы недостижимы
		float x = target.x, y = target.y;
		float gamma = (y != 0) ? atan(x / y) : ::half_pi;

		float a = l2, c = l1, b = length(target);

		float alpha = acos((b*b + c*c - a*a) / (2 * b*c));
		float beta = acos((a*a + c*c - b*b) / (2 * a*c));

		return std::array<float, 2>{ normalizeAngle(::half_pi - gamma - alpha),
			normalizeAngle(::pi - beta) };
	}

	Arm::AnglesTuple AnalyticalSolver::SolveAngles(const Element &chainStart, const glm::vec3 &target) const
	{
		auto *base = dynamic_cast<const Basement*>(&chainStart);
		assert(base != nullptr); // цепочка начинается с основания
		Element *c = chainStart.Child();
		auto *link1 = dynamic_cast<Link*>(c);
		assert(c != nullptr && link1 != nullptr); //, из которого растёт звено
		c = c->Child();
		auto *link2 = dynamic_cast<Link*>(c);
		assert(c != nullptr && link2 != nullptr); //, из которого растёт ещё одно
		assert(c->Child() == nullptr); //, которое последнее в цепочке

		AnglesTuple result(3);

		//определяем, на сколько понадобится довернуть основание
		{
			auto relative{ target - base->GetPos() };
			vec2 relativeProjection{ relative.x, relative.y };
			float relativeProjectionYaw = orientedAngle(vec2{ 1, 0 }, normalize(relativeProjection));
			result[0] = relativeProjectionYaw;
		}

		//трансформируем целевую точку для её использования в плоском солвере
		vec3 targetAligned;
		{
			auto relative{ target - link1->GetPos() };
			vec2 relativeProjection{ relative.x, relative.y };
			float relativeProjectionYaw = orientedAngle(vec2{ 1, 0 }, normalize(relativeProjection));
			targetAligned = rotateZ(relative, -relativeProjectionYaw);
		}

		auto pitchesAligned = solve2d({ targetAligned.x, targetAligned.z },
			link1->GetLength(),
			link2->GetLength());

		result[1] = pitchesAligned[0];
		result[2] = pitchesAligned[1];

		return result;
	}
}
