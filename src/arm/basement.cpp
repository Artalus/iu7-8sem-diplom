#include <stdafx.h>

#include "basement.h"

#include <glm/gtx/rotate_vector.inl>
#include <algorithm>

#include <render/render.h>

using namespace glm;

namespace Arm
{
	Basement::Basement(const glm::vec3 &position, const glm::vec3 &attachmentPoint)
		: Element(nullptr)
		, pos(position)
		, attachment(attachmentPoint)
	{
		clr.u32 = reinterpret_cast<uint32_t>(this);
	}

	vec3 Basement::GetPos() const
	{
		return pos;
	}

	float Basement::GetYaw() const
	{
		return yaw;
	}

	float Basement::GetPitch() const
	{
		return 0;
	}

	float Basement::GetRoll() const
	{
		return 0;
	}

	vec3 Basement::GetAttachmentPoint() const
	{
		return pos + rotateZ(attachment, GetYaw());
	}

	vec3 Basement::GetAttachmentAxis() const
	{
		return rotateZ(vec3(0, -1, 0), GetYaw());
	}

	void Basement::Update(const float timeDelta)
	{
		if ( state == State::Rotating )
		{
			float diff = targetYaw - yaw;
			if ( std::abs(diff) <= rotationPrecision )
			{
				state = State::Idle;
				return; // TODO: промигивает гуи на один кадр
			}
			float rotation = min(abs(diff), yawSpeed*timeDelta) * SIGN(diff);
			yaw += rotation;
		}
	}

	void Basement::Render() const
	{
		glColor3ubv(clr.bytes);
		Render::DrawBox(pos, dimensions, { 0, 0, yaw });
	}

	void Basement::RotateAt(const float targetAngle, const float speed)
	{
		state = State::Rotating;
		targetYaw = targetAngle;
		yawSpeed = speed;
	}
}
