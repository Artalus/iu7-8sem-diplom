#pragma once

#include "IAngleSolver.h"
#include <glm/vec3.hpp>

#include <anfisLib.h>

namespace Arm
{
	using AnglesAndEndpoint = std::pair<AnglesTuple, glm::vec3>;

	class RotationAnfisSolver : public IAngleSolver
	{
		mwArray fisTh1{ 1,1,mxSTRUCT_CLASS },
			fisTh2{ 1,1,mxSTRUCT_CLASS };
		bool ready = false;
	public:
		bool IsReady() { return ready; }

		//загружает структуру FIS из <filename>.fis
		void LoadFis(std::string filenameTh1, std::string filenameTh2);
		AnglesTuple SolveAngles(const Element &chainStart, const glm::vec3 &target) const override;
	};
}
