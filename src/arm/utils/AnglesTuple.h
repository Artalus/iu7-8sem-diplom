#pragma once

#include <vector>
#include <glm/vec3.hpp>

namespace Arm
{

	struct AnglesTuple : std::vector<float>
	{
		AnglesTuple() = default;
		AnglesTuple(size_type size);
		AnglesTuple(std::initializer_list<float> lst);
		
		bool operator==(const AnglesTuple &rhs);
	};


	using AnglesAndEndpoint = std::pair<AnglesTuple, glm::vec3>;
}
