#pragma once
#include <arm/basement.h>
#include "AnglesTuple.h"
#include <glm/gtc/constants.hpp>


std::vector<Arm::AnglesAndEndpoint> CalculateAllowedPoints(Arm::Basement &b,
	float yawmin = -::pi, float yawmax = ::pi,
	float th1min = -::pi, float th1max = ::pi,
	float th2min = -::pi, float th2max = ::pi, float anglestep = ::pi/24.f);

void ExportAllowedPoints(const std::vector<Arm::AnglesAndEndpoint> &aae);