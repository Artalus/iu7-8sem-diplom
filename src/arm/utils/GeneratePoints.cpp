#include <stdafx.h>

#include "GeneratePoints.h"

#include <glm/gtc/constants.hpp>
#include <glm/vec3.hpp>

#include <arm/link.h>
#include "AnglesTuple.h"

#include <fstream>
#include <glm/gtc/type_ptr.hpp>


using namespace glm;
using namespace std;
using namespace Arm;


//th1 - поворот основания, th2 - подъём первого звена, th3 - второго, l3 - длины звеньев
vec3 calculatePosition_analytical_1b2l(float th1, float th2, float L1, float th3, float L2)
{
	float X = L1*cos(th1)*cos(th2)
		+ L2*cos(th2 + th3)*cos(th1);
	float Y = L1*sin(th1)*cos(th2)
		+ L2*cos(th2 + th3)*sin(th1);
	float Z = L1*sin(th2)
		+ L2*sin(th2 + th3);
	return vec3(X, Y, Z);
}

static auto GetAllAnglesAndEndpoints(Basement &b, Link &l1, Link &l2,
	float yawmin, float yawmax, float th1min, float th1max, float th2min, float th2max, float angleStep)
{
	vector<AnglesAndEndpoint> allAAE;
	auto possibilities = static_cast<size_t>(pow(2.f*::pi / angleStep, 3.f));
	allAAE.reserve(possibilities);

	// TODO: потенциально можно распараллелить
	//for ( float th1 = -pi; th1 < pi; th1 += angleStep )
	for ( float th1 = yawmin; th1 < yawmax; th1 += angleStep )
	{
		for ( float th2 = th1min; th2 < th1max; th2 += angleStep )
		{
			for ( float th3 = th2min; th3 < th2max; th3 += angleStep )
			{
				AnglesTuple corrected{ th1,l1.allowedAngle.Correct(th2), l2.allowedAngle.Correct(th3) };
				allAAE.emplace_back(
					corrected,
					l1.GetPos() + calculatePosition_analytical_1b2l(corrected[0], corrected[1], l1.GetLength(), corrected[2], l2.GetLength())
				);
			}
		}
	}

	return allAAE;
}

// TODO: тоже в общий хедер! именно с учетом эпсилона
static bool FloatsEqual(float a, float b, float eps)
{
	return abs(a - b) < eps;
}
static bool AnglesEqual(float a, float b)
{
	static const float eps = 1e-2f;
	return FloatsEqual(a, b, eps);
}

//удалить из вектора всех набранных углов+точек дубликаты
static auto CleanseAAE(const vector<AnglesAndEndpoint> &allAAE)
{
	vector<AnglesAndEndpoint> cleansed;
	cleansed.reserve(allAAE.size());

	/* в дебаге STL дичайше тормозит из-за сонма дополнительных проверок и перепроверок, и на одних
	и тех же входных данных чистильщик начинает отрабатывать 4 минуты вместо 0.2 секунды
	чтобы от этой вакханалии избавиться, сделал две реализации: одна простая для релиза,
	вторая с подвывертами и оптимизациями для дебага:
	range-for внутри на for(i < s) - с 4 минут до 36 секунд
	cleansed[i] на d=data(); d[i] - до 25 секунд
	инлайн == кортежей - до 11 секунд
	вынос дата лхс вовне - до 8 сек
	range-for снаружи на for(i < s) - до 0.3 сек
	*/
#ifndef NDEBUG
	auto alldata = allAAE.data();
	for ( auto s = allAAE.size(), i = 0U; i < s; ++i )
	{
		const auto &angles{ alldata[i].first };
		auto datasize = angles.size();
		auto data_lhs = angles.data();

		bool found = false;

		//find_if с лямбдой был бы в десять раз тормознее
		auto d = cleansed.data();

		for ( auto s = cleansed.size(), i = 0U; i < s; ++i )
		{
			const auto &compare{ d[i].first };

			bool equal = true;
			auto data_rhs = compare.data();
			for ( size_t i = 0; i < datasize; ++i )
				if ( !AnglesEqual(data_lhs[i], data_rhs[i]) )
				{
					equal = false;
					break;
				}

			if ( equal )
			{
				found = true;
				break;
			}
		}

		if ( !found )
			cleansed.push_back(alldata[i]);
	}
#else
	for ( const auto &p : allAAE )
	{
		bool found = false;
		for ( const auto &p_c : cleansed )
			if ( p_c.first == p.first )
			{
				found = true;
				break;
			}

		if ( !found )
			cleansed.push_back(p);
	}
#endif

	cleansed.shrink_to_fit();
	return cleansed;
}


std::vector<Arm::AnglesAndEndpoint> CalculateAllowedPoints(Arm::Basement &b, float yawmin /*= -::pi()*/, float yawmax /*= ::pi()*/, float th1min /*= -::pi()*/, float th1max /*= ::pi()*/, float th2min /*= -::pi()*/, float th2max /*= ::pi()*/, float anglestep /*= ::pi/24.f*/)
{
	// TODO: учитывать общий вид вместо строго заданного
	Link &l1 = *static_cast<Link*>(b.Child());
	Link &l2 = *static_cast<Link*>(l1.Child());

	auto all{ GetAllAnglesAndEndpoints(b, l1, l2, yawmin, yawmax, th1min, th1max, th2min, th2max, anglestep) };
	auto cleansed{ CleanseAAE(all) };

	return cleansed;
}


void ExportAllowedPoints(const std::vector<Arm::AnglesAndEndpoint> &aae)
{
	std::ofstream angles("matlab/exportdata/angles");
	std::ofstream points("matlab/exportdata/points");
	for ( auto p : aae )
	{
		const auto &a = p.first;
		for ( size_t i = 0, s = a.size(); i < s; ++i )
		{
			angles << a[i];
			if ( i < s - 1 )
				angles << "; ";
			else
				angles << std::endl;
		}
		auto v = glm::value_ptr(p.second);
		for ( size_t i = 0, s = p.second.length(); i < s; ++i )
		{
			points << v[i];
			if ( i < s - 1 )
				points << "; ";
			else
				points << std::endl;
		}
	}
}
