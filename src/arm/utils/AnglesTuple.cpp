#include <stdafx.h>

#include "AnglesTuple.h"


// TODO: в класс углов
// учитывает бОльшую погрешность при сравнении углов - примерно 0.57 градуса
static bool AnglesEqual(float a, float b)
{
	static const float eps = 1e-2f;
	return FloatsEqual(a, b, eps);
}


namespace Arm
{

	AnglesTuple::AnglesTuple(size_type size) : vector<float>(size)
	{

	}

	AnglesTuple::AnglesTuple(std::initializer_list<float> lst) : vector<float>(lst)
	{

	}

	// TODO: переквалифицировать в Equals и вызывать из анфиса с переменной точностью?..
	bool AnglesTuple::operator==(const AnglesTuple &rhs)
	{
		// TODO: может это вообще в ассерт или исключение?..
		auto s = size();
		if ( s != rhs.size() )
			return false;

		//чтобы [] не тормозил в дебаге - мы уже прошли проверку на сайз
		auto ld = data();
		auto rd = rhs.data();

		for ( size_t i = 0; i < s; ++i )
			if ( !AnglesEqual(ld[i], rd[i]) )
				return false;
		return true;
	}
}