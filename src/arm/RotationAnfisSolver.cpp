#include <stdafx.h>

#include "RotationAnfisSolver.h"

#include "basement.h"
#include "link.h"
#include <glm/gtx/vector_angle.hpp>


using namespace glm;
using namespace std;
using namespace Arm;


void RotationAnfisSolver::LoadFis(string filenameTh1, string filenameTh2)
{
	ready = false;

	loadFis(1, fisTh1, filenameTh1.c_str());
	loadFis(1, fisTh2, filenameTh2.c_str());

	ready = true;
}

AnglesTuple RotationAnfisSolver::SolveAngles(const Element &chainStart, const vec3 &target) const
{
	if ( !ready )
		throw std::runtime_error("Trying to solve angles with no FIS loaded");

	auto *base = dynamic_cast<const Basement*>(&chainStart);
	assert(base != nullptr); // цепочка начинается с основания
	Element *c = chainStart.Child();
	auto *link1 = dynamic_cast<Link*>(c);
	assert(c != nullptr && link1 != nullptr); //, из которого растёт звено
	c = c->Child();
	auto *link2 = dynamic_cast<Link*>(c);
	assert(c != nullptr && link2 != nullptr); //, из которого растёт ещё одно
	assert(c->Child() == nullptr); //, которое последнее в цепочке

	auto dist = glm::distance(link1->GetPos(), target);
	if ( dist > link1->GetLength() + link2->GetLength()
		|| dist < link1->GetLength() - link2->GetLength() )
		throw std::runtime_error("Target is unreachable");

	/*mwArray xyz(3, 1, mxDOUBLE_CLASS);
	xyz(1) = target.x;
	xyz(2) = target.y;
	xyz(3) = target.z;

	mwArray yaw, th1, th2;// (1, 1, mxDOUBLE_CLASS);
	predictAngle(1, th1, xyz, fisTh1);
	predictAngle(1, th2, xyz, fisTh2);

	float y = yaw(1, 1);
	float t1 = th1(1, 1);
	float t2 = th2(1, 1);

	return{ y, t1, t2 };*/

	AnglesTuple result(3);

	//определяем, на сколько понадобится довернуть основание
	{
		auto relative{ target - base->GetPos() };
		vec2 relativeProjection{ relative.x, relative.y };
		float relativeProjectionYaw = orientedAngle(vec2{ 1, 0 }, normalize(relativeProjection));
		result[0] = relativeProjectionYaw;
	}

	//трансформируем целевую точку для её использования в плоском солвере
	vec3 targetAligned;
	{
		auto relative{ target - link1->GetPos() };
		vec2 relativeProjection{ relative.x, relative.y };
		float relativeProjectionYaw = orientedAngle(vec2{ 1, 0 }, normalize(relativeProjection));
		targetAligned = rotateZ(relative, -relativeProjectionYaw);
	}

	mwArray xz(2, 1, mxDOUBLE_CLASS);
	xz(1) = targetAligned.x;
	xz(2) = targetAligned.z;

	mwArray th1, th2;
	predictAngle(1, th1, xz, fisTh1);
	predictAngle(1, th2, xz, fisTh2);
	result[1] = th1(1, 1);
	result[2] = th2(1, 1);

	return result;
}
