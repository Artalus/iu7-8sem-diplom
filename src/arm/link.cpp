#include <stdafx.h>

#include "link.h"

#include <glm/gtx/rotate_vector.hpp>

#include <render/render.h>
#include <algorithm>

using namespace glm;

namespace Arm
{
	Link::Link(Element *parent, const float length, const AllowedAngle &allowedAngle)
		: Element(parent)
		, length(length)
		, allowedAngle(allowedAngle)
	{
		clr.u32 = reinterpret_cast<uint32_t>(&parent);
	}

	Link::~Link()
	{
	}

	vec3 Link::GetPos() const
	{
		return parent->GetAttachmentPoint();
	}

	float Link::GetYaw() const
	{
		return parent->GetYaw();
	}

	float Link::GetPitch() const
	{
		return parent->GetPitch() + pitch;
	}

	float Link::GetRoll() const
	{
		return parent->GetPitch();
	}

	float Link::GetLength() const
	{
		return length;
	}

	vec3 Link::GetAttachmentPoint() const
	{
		vec3 axis(parent->GetAttachmentAxis());
		vec3 link(length, 0, 0);
		link = rotateZ(link, GetYaw());
		link = rotate(link, GetPitch(), axis);
		return GetPos() + link;
	}

	void Link::Update(const float timeDelta)
	{
		if ( state == State::Rotating )
		{
			float diff = targetPitch - pitch;
			if ( std::abs(diff) <= rotationPrecision )
			{
				state = State::Idle;
				return; // TODO: промигивает гуи на один кадр
			}

			float rotation = min(abs(diff), pitchSpeed*timeDelta) * SIGN(diff);
			pitch += rotation;
			pitch = allowedAngle.Correct(pitch);
		}
	}

	void Link::Render() const
	{
		vec3 start(GetPos());
		vec3 end(GetAttachmentPoint());

		glColor3ubv(clr.bytes);
		Render::DrawBar(start, end, 0.1f);
		
		vec3 dir = rotateZ(vec3(length*1.1, 0, 0), GetYaw());

		float topAngle = parent->GetPitch() + allowedAngle.maxRad;
		float downAngle = parent->GetPitch() + allowedAngle.minRad;
		vec3 top(rotate(dir, topAngle, GetAttachmentAxis()));
		vec3 bottom(rotate(dir, downAngle, GetAttachmentAxis()));
		glColor3f(1.0f, 0.0f, 0.0f);
		Render::DrawLine(start, start + top);
		Render::DrawLine(start, start + bottom);
	}

	void Link::SetPitch(const float p)
	{
		pitch = allowedAngle.Correct(p);
	}

	void Link::RotateAt(const float targetAngle, const float speed)
	{
		state = State::Rotating;
		targetPitch = targetAngle;
		pitchSpeed = speed;
	}

	glm::vec3 Link::GetAttachmentAxis() const
	{
		return parent->GetAttachmentAxis();
	}


	const float Element::rotationPrecision(0.001f);
}
