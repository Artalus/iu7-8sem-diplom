#include <stdafx.h>

#ifdef _WIN32
#include <Windows.h>
#endif
#include <gl/gl.h>
#include <GL/GLU.h>

#include <string>
#include <exception>

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <sdl.h>
#undef main
#include <imgui.h>

#include "imgui_impl_sdl.h"
#include "camera.h"
#include <render/render.h>

#include <arm/basement.h>
#include <arm/link.h>
#include <arm/AnalyticalSolver.h>
#include <arm/AnfisSolver.h>
#include <arm/utils/GeneratePoints.h>

#include <mclmcrrt.h>
#include <arm/RotationAnfisSolver.h>
#include <sstream>

using namespace glm;

class Exception : public std::exception
{
public:
	Exception(std::string s) : exception(s.c_str())
	{
	}
};

class Program
{
	SDL_Window *window = nullptr;
	SDL_GLContext context = nullptr;

	Camera cam;

	Arm::Basement b{ { 0.f,0.f,0.f }, { 0.f, 0.f, 0.f } };
	Arm::Link *l1, *l2;

	int winWidth = 800, winHeight = 600;
	GLfloat clrClear[4] = { 0.0f, 0.1f, 0.2f, 1.0f };

	vec3 target = { -13.5,0.1,7 };
	std::vector<vec3> allowedPoints;
	bool drawAllowedPoints;

	Arm::IAngleSolver *currentSolver{ nullptr };
	Arm::AnalyticalSolver solverAnalyt;
	Arm::AnfisSolver solverANFIS;
	Arm::RotationAnfisSolver solverRotANFIS;
	std::string anfisConfiguration, rotanfisConfiguration;

	enum eSolverType { None, Analytical, ANFIS, RotANFIS }
	solverUsed{ None };

	static std::string nameAnfisConfiguration(std::string folder, int mfcount, int epochcount, int anglestep, bool planar)
	{
		std::stringstream ss;
		ss << folder << "/"
			<< "mf" << mfcount
			<< "_epoch" << epochcount
			<< "_anglestep_pi_" << anglestep
			<< "__anfis" << (planar ? "Planar" : "");
		return ss.str();
	}

	bool isPointAccessible(const glm::vec3 &target) const
	{
		Arm::AnalyticalSolver s;
		if ( target.z < 0 || distance(l1->GetPos(), target) > l1->GetLength() + l2->GetLength() )
			return false;

		auto angles = s.SolveAngles(b, target);
		if ( angles[1] < 0 || angles[2] < 0 )
			return false;

		return true;
	}

	std::string msgboxText, msgboxCaption;
	void AppearMessageBox(std::string caption, std::string msg)
	{
		msgboxCaption = caption;
		msgboxText = msg;
		ImGui::OpenPopup(msgboxCaption.c_str());
	}

	void ParseIO()
	{
		if ( ImGui::IsAnyItemActive() )
			return;

		auto &io = ImGui::GetIO();
		io.KeysDown['w'];
		const vec3
			forward(cam.lspeed, 0, 0),
			left(0, cam.lspeed, 0),
			up(0, 0, cam.lspeed);
		vec3 dir;

		if ( io.KeysDown['w'] )
			dir += forward;
		if ( io.KeysDown['s'] )
			dir -= forward;
		if ( io.KeysDown['a'] )
			dir += left;
		if ( io.KeysDown['d'] )
			dir -= left;
		if ( io.KeysDown[' '] )
			dir += up;
		if ( io.KeysDown['c'] )
			dir -= up;

		if ( io.KeysDown[io.KeyMap[ImGuiKey_LeftArrow]] )
			cam.rot.z += cam.aspeed;
		if ( io.KeysDown[io.KeyMap[ImGuiKey_RightArrow]] )
			cam.rot.z -= cam.aspeed;
		if ( io.KeysDown[io.KeyMap[ImGuiKey_UpArrow]] )
			cam.rot.y -= cam.aspeed;
		if ( io.KeysDown[io.KeyMap[ImGuiKey_DownArrow]] )
			cam.rot.y += cam.aspeed;

		if ( dir != vec3() )
		{
			dir = rotateZ(dir, cam.rot.z);
			cam.pos += dir;
		}
	}

	void UpdateControls()
	{
		static const ImColor selectedsolver_color_norm{ 80, 140, 80 };
		static const ImColor selectedsolver_color_hover{ 120, 180, 120 };
		static const ImColor selectedsolver_color_active{ 60, 100, 60 };
		auto colorizedButton = [this](eSolverType t, const char *buttonText,
			Arm::IAngleSolver *solverAddr)
		{
			bool wasColored = false;
			if ( solverUsed == t )
			{
				wasColored = true;
				ImGui::PushStyleColor(ImGuiCol_Button, selectedsolver_color_norm);
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered, selectedsolver_color_hover);
				ImGui::PushStyleColor(ImGuiCol_ButtonActive, selectedsolver_color_active);
			}

			if ( ImGui::Button(buttonText) )
			{
				if ( solverUsed != t )
				{
					currentSolver = solverAddr;
					solverUsed = t;
				}
				else
				{
					currentSolver = nullptr;
					solverUsed = None;
				}
			}
			
			if ( wasColored )
				ImGui::PopStyleColor(3);
		};


		colorizedButton(Analytical, "Analytical Solver", &solverAnalyt);
		ImGui::SameLine(); colorizedButton(ANFIS, "ANFIS Solver", &solverANFIS);
		ImGui::SameLine(); colorizedButton(RotANFIS, "Rotation-ANFIS Solver", &solverRotANFIS);
		
		if ( solverUsed == ANFIS || solverUsed == RotANFIS )
		{
			bool rotanfisUsed = solverUsed == RotANFIS;
			ImGui::Text("Last chosen FIS configuration:");
			ImGui::Text(rotanfisUsed ? rotanfisConfiguration.c_str() : anfisConfiguration.c_str());

			if ( ImGui::CollapsingHeader("Load new configuration") )
			{
				static char folder[MAX_PATH] = "matlab/trainedfis";
				static int mfcount = 8, epochcount = 200, anglestep = 96;
				static bool planar = true;

				ImGui::InputText("Folder", folder, MAX_PATH);
				ImGui::InputInt("MF count", &mfcount, 1); if ( mfcount < 2 ) mfcount = 2;
				ImGui::InputInt("Epoch count", &epochcount, 50); if ( epochcount < 50 ) epochcount = 50;
				ImGui::InputInt("Pi/anglestep", &anglestep, 12); if ( anglestep > 96 ) anglestep = 96; else if ( anglestep < 24 ) anglestep = 24;

				if ( ImGui::Button("LOAD") )
				{
					try
					{
						if ( rotanfisUsed )
						{
							rotanfisConfiguration = nameAnfisConfiguration(folder, mfcount, epochcount, anglestep, true);
							static_cast<Arm::RotationAnfisSolver*>(currentSolver)->LoadFis(
								rotanfisConfiguration + "_th1",
								rotanfisConfiguration + "_th2");
							AppearMessageBox("Info", "All FIS loaded");
						}
						else
						{
							anfisConfiguration = nameAnfisConfiguration(folder, mfcount, epochcount, anglestep, false);
							static_cast<Arm::AnfisSolver*>(currentSolver)->LoadFis(
								anfisConfiguration + "_yaw",
								anfisConfiguration + "_th1",
								anfisConfiguration + "_th2");
						}
					}
					catch ( mwException &e )
					{
						if ( rotanfisUsed )
							rotanfisConfiguration = "";
						else
							anfisConfiguration = "";
						AppearMessageBox("Loading FIS error", e.what());
					}
				}
			}
		}

		ImGui::Spacing();
		ImGui::Separator();
		ImGui::Spacing();

		ImGui::SliderFloat3("Target", value_ptr(target), -18.f, 18.f);

		if ( ImGui::Button("Solve angles") )
		{
			StartSolving();

		}
	}

	void StartSolving()
	{
		if ( solverUsed == None )
		{
			AppearMessageBox("Solver error", "No solver selected");
			return;
		}
		if ( (solverUsed == ANFIS && !static_cast<Arm::AnfisSolver*>(currentSolver)->IsReady())
			|| (solverUsed == RotANFIS && !static_cast<Arm::RotationAnfisSolver*>(currentSolver)->IsReady()) )
		{
			AppearMessageBox("ANFIS error", "No FIS data loaded");
			return;
		}
		if ( !isPointAccessible(target) )
		{
			AppearMessageBox("Data error", "Target is unreachable");
			return;
		}

		try
		{
			auto result = currentSolver->SolveAngles(b, target);
			b.RotateAt(result[0], 10.f);
			l1->RotateAt(result[1], 10.f);
			l2->RotateAt(result[2], 10.f);
		}
		catch ( std::exception &e )
		{
			AppearMessageBox("Solving Error", std::string{ "Couldn't solve angles for this point:\n" }
							+std::string{ e.what() });
		}
	}

	void UpdateDebugParameters()
	{
		ImGui::Separator();

		if ( ImGui::CollapsingHeader("Points generation") )
		{
			static float yawmin = 0.f, yawmax = 1e-6f,
				th1min = l1->allowedAngle.minRad, th1max = l1->allowedAngle.maxRad,
				th2min = l2->allowedAngle.minRad, th2max = l2->allowedAngle.maxRad,
				anglestep = ::pi / 24.f, anglemin = ::pi / 96.f;
			static auto AngleLimits = [](const char *text, float *min, float *max)
			{
				ImGui::Columns(3, text);
				ImGui::Text(text); ImGui::NextColumn();
				ImGui::PushID(text);
				ImGui::SliderAngle("", min, -180.f, 180.f); ImGui::NextColumn();
				ImGui::PushID(text);
				ImGui::SliderAngle("", max, -180.f, 180.f); ImGui::NextColumn();
				ImGui::PopID();
				ImGui::PopID();
				ImGui::Columns(1, text);
			};
			AngleLimits("Basement rot. limits: ", &yawmin, &yawmax);
			AngleLimits("First link rot. limits: ", &th1min, &th1max);
			AngleLimits("Second link rot. limits:", &th2min, &th2max);
			ImGui::InputFloat("Angle step", &anglestep, anglemin, 0.0f, 6);
			if ( anglestep <= anglemin )
				anglestep = anglemin;
			if ( ImGui::Button("Generate allowed points") )
			{
				auto &&calculated = CalculateAllowedPoints(b, yawmin, yawmax, th1min, th1max, th2min, th2max, anglestep);
				ExportAllowedPoints(calculated);
				allowedPoints.clear();
				allowedPoints.reserve(calculated.size());
				for ( auto s = calculated.size(), i = 0U; i < s; ++i )
					allowedPoints.push_back(calculated[i].second);
				AppearMessageBox("Info", "Done");
			}

			ImGui::Checkbox("Draw generated points", &drawAllowedPoints);
		}

		if ( ImGui::CollapsingHeader("Camera settings") )
		{
			ImGui::SliderFloat3("cam.pos", value_ptr(cam.pos), -10.0f, 10.0f);
			ImGui::SliderFloat3("cam.rot", value_ptr(cam.rot), -180.0f, 180.0f);
			ImGui::SliderFloat("cam.lspeed", &cam.lspeed, 0, 2.0f);
			ImGui::SliderFloat("cam.aspeed", &cam.aspeed, 0, 10.0f);
		}

		if ( ImGui::CollapsingHeader("Element settings") )
		{
			ImGui::Text("Basement");
			{
				static float tg = 0, sp = 1;
				ImGui::SliderAngle("Target yaw", &tg);
				ImGui::SliderFloat("Rotation speed", &sp, 0, 50);
				if ( ImGui::Button("Rotate") )
					b.RotateAt(tg, sp);
			}

			ImGui::Text("Link 1");
			{
				ImGui::PushID(&l1);
				static float tg = 0, sp = 1;
				ImGui::SliderAngle("Target pitch", &tg);
				ImGui::SliderFloat("Rotation speed", &sp, 0, 50);
				if ( ImGui::Button("Rotate") )
					l1->RotateAt(tg, sp);
				ImGui::PopID();
			}

			ImGui::Text("Link 2");
			{
				ImGui::PushID(&l2);
				static float tg = 0, sp = 1;
				ImGui::SliderAngle("Target pitch", &tg);
				ImGui::SliderFloat("Rotation speed", &sp, 0, 50);
				if ( ImGui::Button("Rotate") )
					l2->RotateAt(tg, sp);
				ImGui::PopID();
			}
		}
	}

public:
	Program()
	{
		/*
		ограничение чтобы при максимальных углах поворота получалась конструкция
		__
		 /
		/
		*/
		/*float len1 = 10.f, len2 = 7.f;
		float th1max = ::half_pi - asin(len2 / len1);
		float th2max = ::pi - th1max;*/
		l1 = new Arm::Link{ &b, 10.f, {0, 43} };
		l2 = new Arm::Link{ l1, 7.f, {0, 133} };
	}

	~Program()
	{
	}

	void Init()
	{
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
			throw Exception("Unable to init SDL: " + std::string(SDL_GetError()));

		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		window = SDL_CreateWindow("Manipulator",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			winWidth, winHeight,
			SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_CAPTURE);

		if (!window)
			throw Exception("Unable to create SDL window: " + std::string(SDL_GetError()));

		context = SDL_GL_CreateContext(window);
		SDL_GL_SetSwapInterval(1); //вертикальная синхронизация

		glClearColor(clrClear[0], clrClear[1], clrClear[2], clrClear[3]);
		glClearDepth(1.0);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_CULL_FACE);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(75.0f, static_cast<float>(winWidth) / winHeight, 0.1f, 100.0f);
		//повернём камеру, чтобы смотрела вдоль Y, а затем вдоль X: стандартный угол, когда поворот == 0 градусов
		glRotatef(-90, 1, 0, 0); //поднимем вверх
		glRotatef(90, 0, 0, 1); //повернем направо


		ImGui_ImplSdl_Init(window);
		// in case of russisch nadpises
		/*auto &io = ImGui::GetIO();
		io.Fonts->AddFontFromFileTTF("res/fonts/Arial.ttf", 13.0f, NULL, io.Fonts->GetGlyphRangesCyrillic());*/
		ImGui_ImplSdl_NewFrame(window);
	}
	void Shutdown()
	{
		ImGui_ImplSdl_Shutdown();
		SDL_GL_DeleteContext(context);
		SDL_DestroyWindow(window);
		SDL_Quit();
	}

	void ParseEvent(SDL_Event &e)
	{
		ImGui_ImplSdl_ProcessEvent(&e);
	}

	void StepLogic()
	{
		ParseIO();

		ImGui::Begin("Controls", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
		UpdateControls();

		if ( ImGui::BeginPopupModal(msgboxCaption.c_str()) )
		{
			ImGui::Text(msgboxText.c_str());
			if ( ImGui::Button("Close") )
			{
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
		ImGui::End();

		UpdateDebugParameters();

		b.Update(0.01f);
		l1->Update(0.01f);
		l2->Update(0.01f);
	}

	void StepRender()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glRotatef(-degrees(cam.rot.x), 1, 0, 0);
		glRotatef(-degrees(cam.rot.y), 0, 1, 0);
		glRotatef(-degrees(cam.rot.z), 0, 0, 1);
		glTranslatef(-cam.pos.x, -cam.pos.y, -cam.pos.z);


		b.Render();
		l1->Render();
		l2->Render();


		Render::DrawGizmo(target, 1.f, 2.f);

		if ( drawAllowedPoints )
			Render::DrawPoints(allowedPoints, { 1.f, 0.f, 0.f, 1.0f });

		Render::DrawGrid();
		ImGui::Render();

		auto err = glGetError();
		if ( err )
		{
			throw Exception("OpenGL error: " + std::to_string(err));
		}
		SDL_GL_SwapWindow(window);
		ImGui_ImplSdl_NewFrame(window);
	}
};


int main(int argc, const char* argv[])
{
#ifdef _WIN32
#ifdef _DEBUG
SetCurrentDirectory("../../");
#else
#error ничего не забыл?
#endif
#else
#error
#endif
	mclmcrInitialize();
	mclInitializeApplication(argv, argc);
	anfisLibInitialize();
	try
	{
		Program p;
		p.Init();
		bool running = true;

		while (running)
		{
			SDL_Event e;
			while (SDL_PollEvent(&e))
			{
				p.ParseEvent(e);
				if (e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE))
					running = false;
			}
			p.StepLogic();
			p.StepRender();

			auto err = glGetError();
			if (err)
			{
				printf("!!! gl error: %d\n", err);
			}
		}
		p.Shutdown();
	}
	catch (std::exception &e)
	{
		SDL_ShowSimpleMessageBox(0, "ERROR", e.what(), nullptr);
	}
	catch (...)
	{
		SDL_ShowSimpleMessageBox(0, "ERROR", "A wild something appears!", nullptr);
	}
	anfisLibTerminate();
	mclTerminateApplication();
	return 0;
}
