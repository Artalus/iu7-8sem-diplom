#pragma once

#include <glm/fwd.hpp>

class Camera
{
public:
	glm::vec3 pos;
	glm::vec3 rot;
	float lspeed = 0.1f, aspeed = 0.01f;
	// TODO: сделать апдейт
	// TODO: завязать апдейт на таймер
};