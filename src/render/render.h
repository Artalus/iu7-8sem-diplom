#pragma once

#include <glm/fwd.hpp>
#include <vector>

namespace Render
{
	void DrawBox(const glm::vec3 &center, const glm::vec3 &dimensions, const glm::vec3 &rotation);

	void DrawGizmo(const glm::vec3 &point, float radius, float width);

	void DrawBar(const glm::vec3 &from, const glm::vec3 &to, float width);

	void DrawLine(const glm::vec3 &from, const glm::vec3 &to);

	void DrawGrid();

	void DrawPoints(const std::vector<glm::vec3> &points, const glm::vec4 &color);
}
