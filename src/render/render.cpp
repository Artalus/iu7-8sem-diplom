#include <stdafx.h>

#include "render.h"

#include <Windows.h>
#include <gl/GL.h>

#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assert.h>
#include <vector>

using namespace glm;

namespace Render
{
	class MatrixStacker // TODO: убедиться, что оптимизатор его не вырежет как неиспользуемую переменную
	{
	public:
		MatrixStacker()
		{
			glPushMatrix();
		}
		~MatrixStacker()
		{
			glPopMatrix();
		};
	};
	class AttribStacker
	{
	public:
		AttribStacker(GLbitfield mask)
		{
			glPushAttrib(mask);
		}
		~AttribStacker()
		{
			glPopAttrib();
		}
	};


	static void glVerticesQuad(const float leftUp[3], const float leftDown[3], const float rightDown[3], const float rightUp[3])
	{
		glVertex3fv(leftUp);
		glVertex3fv(leftDown);
		glVertex3fv(rightDown);
		glVertex3fv(rightUp);
	}

	//////////////////////////////////////////////////////////////////////////

	void DrawBox(const vec3 &center, const vec3 &dimensions, const vec3 &rotation)
	{
		MatrixStacker m;
		glTranslatef(center.x, center.y, center.z);

		glRotatef(degrees(rotation.z), 0, 0, 1);
		glRotatef(degrees(rotation.y), 0, 1, 0);
		glRotatef(degrees(rotation.x), 1, 0, 0);

		static const struct
		{
			const float flu[3] = { 1, 1, 1 }, // front left up
				fld[3] = { 1, 1, -1 },        // front left down
				fru[3] = { 1, -1, 1 },        // ...
				frd[3] = { 1, -1, -1 },

				blu[3] = { -1, 1, 1 },
				bld[3] = { -1, 1, -1 },
				bru[3] = { -1, -1, 1 },
				brd[3] = { -1, -1, -1 };
		} v;

		glScalef(dimensions.x / 2, dimensions.y / 2, dimensions.z / 2);

		glBegin(GL_QUADS);
		glVerticesQuad(v.flu, v.blu, v.bru, v.fru); // верхняя грань
		glVerticesQuad(v.brd, v.bld, v.fld, v.frd); // нижняя грань
		glVerticesQuad(v.flu, v.fru, v.frd, v.fld); // передняя грань
		glVerticesQuad(v.blu, v.bld, v.brd, v.bru); // задняя грань
		glVerticesQuad(v.flu, v.fld, v.bld, v.blu); // левая грань
		glVerticesQuad(v.bru, v.brd, v.frd, v.fru); // правая грань
		glEnd();
	}

	void DrawGizmo(const vec3 &point, float radius, float width)
	{
		const vec3 x(radius, 0, 0), y(0, radius, 0), z(0, 0, radius);

		AttribStacker a(GL_CURRENT_BIT | GL_LINE_BIT); // RGBA + linewidth
		glLineWidth(width);

		glBegin(GL_LINES);
		glColor3f(1, 0, 0);
		glVertex3fv(value_ptr(point - x));
		glVertex3fv(value_ptr(point + x));
		glColor3f(0, 1, 0);
		glVertex3fv(value_ptr(point - y));
		glVertex3fv(value_ptr(point + y));
		glColor3f(0, 0, 1);
		glVertex3fv(value_ptr(point - z));
		glVertex3fv(value_ptr(point + z));
		glEnd();
	}


	static float getYaw(const vec3 &v)
	{
		assert(v.x != 0 || v.y != 0 || v.z != 0);

		if ( v.x != 0 || v.y != 0 )
			return atan2(v.y, v.x);
		else
			return 0; // TODO: приводит к тому, что вертикальные балки всегда ориентированы по X+
	}
	static float getPitch(const vec3 &v)
	{
		static const float pi2 = ::pi / 2.0f;
		assert(v.x != 0 || v.y != 0 || v.z != 0);

		if ( v.x != 0 || v.y != 0 )
			return -atan(v.z / sqrt(v.x*v.x + v.y*v.y));

		if ( v.z > 0 )
			return -pi2;
		else
			return pi2;


	}
	void DrawBar(const vec3 &from, const vec3 &to, float width)
	{
		vec3 dir(normalize(to - from));

		float yaw = getYaw(dir);
		float pitch = getPitch(dir);

		vec3 center((from + to) / 2.0f);
		DrawBox(center,
			vec3(length(to - from), width, width),
			vec3(0, pitch, yaw));
	}

	void DrawLine(const vec3 &from, const vec3 &to)
	{
		glBegin(GL_LINES);
		glVertex3fv(value_ptr(from));
		glVertex3fv(value_ptr(to));
		glEnd();
	}

	void DrawGrid()
	{
		static const float gridRadius = 500.0f;
		static const float gridStep = 1.0f;

		AttribStacker a(GL_CURRENT_BIT | GL_POLYGON_BIT | GL_LIGHTING_BIT); // RGBA + cullface + lighting
		glDisable(GL_CULL_FACE);
		glDisable(GL_LIGHTING);

		MatrixStacker m;
		//сетка
		glColor4f(0, 1, 1, 0.5f);
		glBegin(GL_LINES);
		for ( float i = -gridRadius; i <= gridRadius + gridStep / 2.0f; i += gridStep )
		{
			if ( i != 0 )
			{
				glVertex2f(i, -gridRadius);
				glVertex2f(i, gridRadius);
				glVertex2f(-gridRadius, i);
				glVertex2f(gridRadius, i);
			}
		}
		glEnd();

		//плоскость
		glColor4f(0.1f, 0.2f, 0.3f, 0.5f);
		glBegin(GL_QUADS);
		glVertex3f(-gridRadius, gridRadius, 0);
		glVertex3f(-gridRadius, -gridRadius, 0);
		glVertex3f(gridRadius, -gridRadius, 0);
		glVertex3f(gridRadius, gridRadius, 0);
		glEnd();

		//координатные оси
		DrawGizmo({ 0,0,0 }, gridRadius, 2.0f);
	}

	void DrawPoints(const std::vector<vec3> &points, const vec4 &color)
	{
		AttribStacker a(GL_CURRENT_BIT | GL_POINT_BIT); // RGBA + pointsize

		glColor4fv(value_ptr(color));
		glPointSize(2.f);
		glBegin(GL_POINTS);
		for ( auto s = points.size(), i = 0U; i < s; ++i )
			glVertex3fv(value_ptr(points[i]));
		glEnd();
	}
}