#include <stdafx.h>

#include <arm/AnalyticalSolver.h>

#include <glm/vec2.hpp>
#include <glm/gtc/constants.inl>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	using namespace Arm;

	TEST_CLASS(AnalyticalSolverTests)
	{
	public:
		TEST_METHOD(Solve2d_length1_point1x1)
		{
			auto result = AnalyticalSolver::solve2d({ 1,1 }, 1, 1);
			Assert::IsTrue(
				FloatsEqual(result[0], 0.) &&
				FloatsEqual(result[1], half_pi)
			);
		}

		TEST_METHOD(Solve2d_length1_point2x0)
		{
			auto result = AnalyticalSolver::solve2d({ 2,0 }, 1, 1);
			Assert::IsTrue(
				FloatsEqual(result[0], 0.) &&
				FloatsEqual(result[1], 0.)
			);
		}

		TEST_METHOD(Solve2d_length2_point2x2)
		{
			auto result = AnalyticalSolver::solve2d({ 2,2 }, 2, 2);
			Assert::IsTrue(
				FloatsEqual(result[0], 0.) &&
				FloatsEqual(result[1], half_pi)
			);
		}

		TEST_METHOD(Solve2d_length2_point4x0)
		{
			auto result = AnalyticalSolver::solve2d({ 4,0 }, 2, 2);
			Assert::IsTrue(
				FloatsEqual(result[0], 0.) &&
				FloatsEqual(result[1], 0.)
			);
		}

		//p = solve2d({ 1,0 }, 1, 1);
		//p = solve2d({ 0,1 }, 1, 1);
	};
}