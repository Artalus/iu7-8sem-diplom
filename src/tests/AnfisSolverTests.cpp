#include <stdafx.h>

#include <arm/AnfisSolver.h>

#include <arm/element.h>
#include <arm/basement.h>
#include <arm/link.h>
#include <arm/utils/GeneratePoints.h>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

glm::vec3 calculatePosition_analytical_1b2l(float th1, float th2, float L1, float th3, float L2);

namespace Tests
{
	using namespace glm;
	using namespace std;
	using namespace Arm;

	AnfisSolver GenerateTestSolver()
	{
		return AnfisSolver{};
	}

	unique_ptr<Basement> GenerateTestManipulator()
	{
		auto b = make_unique<Basement>( vec3{0.f, 0.f, 0.f}, vec3{0.f, 0.f, 0.f} );
		Link *l1 = new Link(b.get(), 10.f, {0, 90.f});
		Link *l2 = new Link(l1, 7.f, {0, 180.f});
		return b;
	}

	inline void CheckSize(const AnglesTuple &result)
	{
		Assert::AreEqual(3U, result.size(), L"Anfis solver should return 3 angles");
	}
	
	

	TEST_CLASS(AnfisSolverTests)
	{
	public:

		TEST_METHOD(CalculatePos_0x0x0_1_1)
		{
			auto res = calculatePosition_analytical_1b2l(0, 0, 1, 0, 1);
			bool config = true
				&& FloatsEqual(res.x, 2.f)
				&& FloatsEqual(res.y, 0.f)
				&& FloatsEqual(res.z, 0.f)
				;
			Assert::IsTrue(config);
		}

		TEST_METHOD(CalculatePos_0x0x0_2_2)
		{
			auto res = calculatePosition_analytical_1b2l(0, 0, 2, 0, 2);
			bool config = true
				&& FloatsEqual(res.x, 4.f)
				&& FloatsEqual(res.y, 0.f)
				&& FloatsEqual(res.z, 0.f)
				;
			Assert::IsTrue(config);
		}

		TEST_METHOD(CalculatePos_Pi2x0x0_1_1)
		{
			auto res = calculatePosition_analytical_1b2l(::half_pi, 0, 1, 0, 1);
			bool config = true
				&& FloatsEqual(res.x, 0.f)
				&& FloatsEqual(res.y, 2.f)
				&& FloatsEqual(res.z, 0.f)
				;
			Assert::IsTrue(config);
		}

		TEST_METHOD(CalculatePos_0xPi2x0_1_1)
		{
			auto res = calculatePosition_analytical_1b2l(0, ::half_pi, 1, 0, 1);
			bool config = true
				&& FloatsEqual(res.x, 0.f)
				&& FloatsEqual(res.y, 0.f)
				&& FloatsEqual(res.z, 2.f)
				;
			Assert::IsTrue(config);
		}

		TEST_METHOD(CalculatePos_0x0xPi2_1_1)
		{
			auto res = calculatePosition_analytical_1b2l(0, 0, 1, ::half_pi, 1);
			bool config = true
				&& FloatsEqual(res.x, 1.f)
				&& FloatsEqual(res.y, 0.f)
				&& FloatsEqual(res.z, 1.f)
				;
			Assert::IsTrue(config);
		}

		TEST_METHOD(AllowedPoints_are_unique)
		{
			auto m = GenerateTestManipulator();
			auto calculated = CalculateAllowedPoints(*m);

			Assert::IsTrue(calculated.size() > 0, L"There should be at least one allowed AAE");

			vector<AnglesTuple> unique;
			unique.reserve(calculated.size());

			for ( auto p : calculated )
				unique.push_back(p.first);

			vector<AnglesTuple>::iterator i, j;
			for ( i = unique.begin(); i != unique.end(); ++i )
				for ( j = i + 1; j != unique.end(); ++j )
					if ( *i == *j )
						Assert::Fail(L"Allowed AAE set should contain only unique tuples");
		}

		TEST_METHOD(Solve_2x0x0)
		{
			auto result = GenerateTestSolver().SolveAngles(*GenerateTestManipulator(), { 2.f, 0.f, 0.f });
			CheckSize(result);

			Assert::IsTrue(result == AnglesTuple{0.f, 0.f, 0.f}, L"This case should be solved as 0,0,0");
		}

		TEST_METHOD(Solve_0x2x0)
		{
			auto result = GenerateTestSolver().SolveAngles(*GenerateTestManipulator(), { 0.f, 2.f, 0.f });
			CheckSize(result);

			Assert::IsTrue(result == AnglesTuple{ ::half_pi, 0.f, 0.f }, L"This case should be solved as 0,0,0");
		}

		TEST_METHOD(Solve_0x0x2)
		{
			auto result = GenerateTestSolver().SolveAngles(*GenerateTestManipulator(), { 0.f, 2.f, 0.f });
			CheckSize(result);

			bool config = // |
				FloatsEqual(result[1], 0.) &&
				FloatsEqual(result[2], 0.);

			Assert::IsTrue(config, L"This case should be solved as ??,Pi/2,0");
		}

		TEST_METHOD(Solve_1x0x1)
		{
			auto result = GenerateTestSolver().SolveAngles(*GenerateTestManipulator(), { 1.f, 0.f, 1.f });
			CheckSize(result);
			
			bool config1 = (result == AnglesTuple{ 0.f, 0.f, ::half_pi }); // _|
			bool config2 = (result == AnglesTuple{ 0, ::half_pi, -::half_pi }); // |-

			Assert::IsTrue(config1 || config2, L"This case should be solved as 0,0,Pi/2  or  0,Pi/2,-Pi/2");
		}
	};
}