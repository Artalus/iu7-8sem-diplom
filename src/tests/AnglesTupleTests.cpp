#include <stdafx.h>

#include <arm/utils/AnglesTuple.h>

#include <arm/element.h>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	using glm::radians;
	using namespace std;
	using namespace Arm;

	TEST_CLASS(AnglesTupleTests)
	{
	public:

		TEST_METHOD(Compare_0x0x0_with_0x0x0)
		{
			AnglesTuple lhs{ 0.f, 0.f, 0.f };
			AnglesTuple rhs{ 0.f, 0.f, 0.f };
			Assert::IsTrue(lhs == rhs, L"These tuples should be equal");
		}

		TEST_METHOD(Compare_0x0x0_with_1x2x3)
		{
			AnglesTuple lhs{ 0.f, 0.f, 0.f };
			AnglesTuple rhs{ 1.1f, 2.2f, 3.3f };
			Assert::IsFalse(lhs == rhs, L"These tuples should not be equal");
		}

		TEST_METHOD(Compare_1x2x3_with_1x2x3)
		{
			AnglesTuple lhs{ 1.1f, 2.2f, 3.3f };
			AnglesTuple rhs{ 1.1f, 2.2f, 3.3f };
			Assert::IsTrue(lhs == rhs, L"These tuples should be equal");
		}

		TEST_METHOD(Compare_1x2x3_with_3x2x1)
		{
			AnglesTuple lhs{ 1.f, 2.f, 3.f };
			AnglesTuple rhs{ 3.f, 2.f, 1.f };
			Assert::IsFalse(lhs == rhs, L"These tuples should not be equal");
		}

		TEST_METHOD(Compare_Sin_with_Sin)
		{
			AnglesTuple lhs{ sin(1.f), sin(2.f), sin(3.f) };
			AnglesTuple rhs{ sin(1.f), sin(2.f), sin(3.f) };
			Assert::IsTrue(lhs == rhs, L"These tuples should be equal");
		}

		TEST_METHOD(Compare_difference1e_3)
		{
			AnglesTuple lhs{ radians(30.0f), 0.f, 0.f };
			AnglesTuple rhs{ radians(30.001f), 0.f, 0.f };
			Assert::IsTrue(lhs == rhs, L"These tuples should be equal");
		}

		TEST_METHOD(Compare_difference1e_2)
		{
			AnglesTuple lhs{ radians(30.0f), 0.f, 0.f };
			AnglesTuple rhs{ radians(30.01f), 0.f, 0.f };
			Assert::IsTrue(lhs == rhs, L"These tuples should be equal");
		}

		TEST_METHOD(Compare_difference1e_1)
		{
			AnglesTuple lhs{ radians(30.f), 0.f, 0.f };
			AnglesTuple rhs{ radians(30.1f), 0.f, 0.f };
			Assert::IsTrue(lhs == rhs, L"These tuples should be equal");
		}

		TEST_METHOD(Compare_difference1e1)
		{
			AnglesTuple lhs{ sin(radians(30.f)), 0.f, 0.f };
			AnglesTuple rhs{ sin(radians(31.f)), 0.f, 0.f };
			Assert::IsFalse(lhs == rhs, L"These tuples should not be equal");
		}
	};
}