#include "stdafx_common.h"

#include <glm/gtc/constants.hpp>

float half_pi{ glm::half_pi<float>() };
float pi{ glm::pi<float>() };
float two_pi{ glm::two_pi<float>() };

bool FloatsEqual(float a, float b, float eps)
{
	return abs(a - b) < eps;
}
