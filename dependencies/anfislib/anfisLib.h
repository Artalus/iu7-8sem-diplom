//
// MATLAB Compiler: 6.1 (R2015b)
// Date: Mon May 30 12:59:55 2016
// Arguments: "-B" "macro_default" "-W" "cpplib:anfisLib" "-T" "link:lib" "-d"
// "P:\git\iu7-8sem-diplom\matlab\anfisLib\for_testing" "-v"
// "P:\git\iu7-8sem-diplom\matlab\loadFis.m"
// "P:\git\iu7-8sem-diplom\matlab\predictAngle.m" 
//

#ifndef __anfisLib_h
#define __anfisLib_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#include "mclcppclass.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_anfisLib
#define PUBLIC_anfisLib_C_API __global
#else
#define PUBLIC_anfisLib_C_API /* No import statement needed. */
#endif

#define LIB_anfisLib_C_API PUBLIC_anfisLib_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_anfisLib
#define PUBLIC_anfisLib_C_API __declspec(dllexport)
#else
#define PUBLIC_anfisLib_C_API __declspec(dllimport)
#endif

#define LIB_anfisLib_C_API PUBLIC_anfisLib_C_API


#else

#define LIB_anfisLib_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_anfisLib_C_API 
#define LIB_anfisLib_C_API /* No special import/export declaration */
#endif

extern LIB_anfisLib_C_API 
bool MW_CALL_CONV anfisLibInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_anfisLib_C_API 
bool MW_CALL_CONV anfisLibInitialize(void);

extern LIB_anfisLib_C_API 
void MW_CALL_CONV anfisLibTerminate(void);



extern LIB_anfisLib_C_API 
void MW_CALL_CONV anfisLibPrintStackTrace(void);

extern LIB_anfisLib_C_API 
bool MW_CALL_CONV mlxLoadFis(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_anfisLib_C_API 
bool MW_CALL_CONV mlxPredictAngle(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_anfisLib_C_API 
bool MW_CALL_CONV mlxPredictAnglePlanar(int nlhs, mxArray *plhs[], int nrhs, mxArray 
                                        *prhs[]);


#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

/* On Windows, use __declspec to control the exported API */
#if defined(_MSC_VER) || defined(__BORLANDC__)

#ifdef EXPORTING_anfisLib
#define PUBLIC_anfisLib_CPP_API __declspec(dllexport)
#else
#define PUBLIC_anfisLib_CPP_API __declspec(dllimport)
#endif

#define LIB_anfisLib_CPP_API PUBLIC_anfisLib_CPP_API

#else

#if !defined(LIB_anfisLib_CPP_API)
#if defined(LIB_anfisLib_C_API)
#define LIB_anfisLib_CPP_API LIB_anfisLib_C_API
#else
#define LIB_anfisLib_CPP_API /* empty! */ 
#endif
#endif

#endif

extern LIB_anfisLib_CPP_API void MW_CALL_CONV loadFis(int nargout, mwArray& fismat, const mwArray& filename);

extern LIB_anfisLib_CPP_API void MW_CALL_CONV predictAngle(int nargout, mwArray& angle, const mwArray& XYZ, const mwArray& fis);

extern LIB_anfisLib_CPP_API void MW_CALL_CONV predictAnglePlanar(int nargout, mwArray& angle, const mwArray& XZ, const mwArray& fis);

#endif
#endif
